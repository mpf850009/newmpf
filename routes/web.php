<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'NewmpfController@index')->name('index');
Route::get('/news', 'NewmpfController@news')->name('news');
Route::get('/news/{date}', 'NewmpfController@news_date')->name('news_date');
Route::get('/company', 'NewmpfController@company')->name('company');
Route::get('/product', 'NewmpfController@product')->name('product');
Route::get('/product/{series}', 'NewmpfController@product_series')->name('product_series');
Route::get('/partner', 'NewmpfController@partner')->name('partner');
Route::get('/contact', 'NewmpfController@contact')->name('contact');
Route::get('/rentalmap', 'NewmpfController@rentalmap')->name('rentalmap');
Route::get('/rentalmap/{rentalplace}', 'NewmpfController@rentalplace')->name('rentalplace');
Route::get('/series', 'NewmpfController@series')->name('series');
Route::get('/tech', 'NewmpfController@tech')->name('tech');

Route::get('/tech/{series}', 'NewmpfController@tech_series')->name('tech_series');

Route::get('/test', 'NewmpfController@test')->name('test');
