<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewmpfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = "index";
        $title  = "首頁";
        return view('newmpf.index', compact('active', 'title'));

    }
    public function company()
    {
        $active = "company";
        $title  = "公司簡介";
        return view('newmpf.company', compact('active', 'title'));

    }
    public function news()
    {
        $active = "news";
        $title  = "最新消息";
        return view('newmpf.news', compact('active', 'title'));

    }
    public function news_date($date)
    {
        $active = "news1";
        $title  = "最新消息";
        return view('newmpf.news_date', compact('active', 'title', 'date'));

    }
    public function product()
    {
        $active = "product";
        $title  = "產品介紹";
        return view('newmpf.product', compact('active', 'title'));

    }
    public function product_series($series)
    {
        $active = "product1";
        $title  = "產品介紹";
        return view('newmpf.product_series', compact('active', 'title', 'series'));

    }
    public function partner()
    {
        $active = "partner";
        $title  = "合作夥伴";
        return view('newmpf.partner', compact('active', 'title'));

    }
    public function contact()
    {
        $active = "contact";
        $title  = "聯絡我們";
        return view('newmpf.contact', compact('active', 'title'));
    }
    public function rentalmap()
    {
        $active = "rentalmap";
        $title  = "全省租車點";
        return view('newmpf.rentalmap', compact('active', 'title'));
    }
    public function rentalplace($place)
    {
        $active = "rentalplace";
        $title  = "全省租車點";
        if ($place == 'fulong') {
            $placename = '新北福隆驛站';
        } elseif ($place == 'yongkang') {
            $placename = '台南永康樂享學';
        } elseif ($place == 'yuli') {
            $placename = '花蓮玉里站';
        } elseif ($place == 'chishang') {
            $placename = '台東池上站';
        } elseif ($place == 'asheng') {
            $placename = '台東阿勝租車';
        } elseif ($place == 'fengyuan') {
            $placename = '台中豐原起站租車';
        } elseif ($place == 'houli') {
            $placename = '后里奇典租車';
        } elseif ($place == 'zuojhen') {
            $placename = '台南左鎮公舘社區發展協會';
        } else {
            $placename = '新增租車地點';
        }
        return view('newmpf.rentalmap_place', compact('active', 'title', 'place', 'placename'));
    }
    public function tech()
    {
        $active = "tech";
        $title  = "技術支援";
        return view('newmpf.tech', compact('active', 'title'));
    }
    public function series()
    {
        $active = "series";
        $title  = "產品總覽";
        return view('newmpf.series', compact('active', 'title'));
    }
    public function tech_series($series)
    {
        $active = "tech1";
        $title  = "技術支援-MPF" . $series;
        return view('newmpf.tech_series', compact('active', 'title', 'series'));

    }
    public function test()
    {
        $active = "test";
        $title  = "測試";
        return view('newmpf.test', compact('active', 'title'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
