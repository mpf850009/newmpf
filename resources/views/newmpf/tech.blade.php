@extends('newmpf.main')
@section('content')

<!-- tech -->
<section class="team py-4 py-lg-5" id="tech">
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">Easy to turn on MPF DRIVE</h3>
            <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">Display AF & Display I4US</p>
        </div>
        <div class="col-12 row pt-sm-5 pt-4" style="padding:0 0;">
            <div class="col-sm-6" style="text-align:center;" style="padding:0 0;">
                <h4 class="wow fadeInUp">AF</h4><br>
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/i8tmPLvrdJU?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-sm-6" style="text-align:center;" style="padding:0 0;">
                <h4 class="wow fadeInUp">I4US</h4><br>
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/W1Xnn16D3VU?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">technical support</h3>
            <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">ALL NEW MPF Drive</p>
        </div>
        <div class="row container text-left pt-sm-5 pt-4">
            <div class="col-sm-6" style="text-align:center;">
                {{-- <h4 class="wow fadeInUp" data-wow-duration="2s">Easy to turn on MPF DRIVE</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="turnonaf"])}}#tech"> AF </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="turnoni4us"])}}#tech"> I4US </a></h5><br> --}}

                {{-- <h4 class="wow fadeInUp" data-wow-duration="2s">MPF3 Series</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="3"])}}#tech"> MPF3 </a></h5><br> --}}

                <h4 class="wow fadeInUp" data-wow-duration="2s">MPF5 Series</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="5"])}}#tech"> MPF5.3 </a></h5><br>
                <h4 class="wow fadeInUp" data-wow-duration="2s">MPF6 Series</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="6C"])}}#tech"> MPF6 C </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="6LT"])}}#tech"> MPF6 LT </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="6S"])}}#tech"> MPF6 S </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="6SL"])}}#tech"> MPF6 SL </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="6ST"])}}#tech"> MPF6 ST </a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="648V"])}}#tech"> MPF6 48V </a></h5><br>
            </div>
            <div class="col-sm-6" style="text-align:center;">
                <h4 class="wow fadeInUp" data-wow-duration="2s">Display AF</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="af"])}}#tech"> AF </a></h5><br>
                <h4 class="wow fadeInUp" data-wow-duration="2s">Display I4U</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="i4u"])}}#tech"> I4U </a></h5><br>
                <h4 class="wow fadeInUp" data-wow-duration="2s">Display I4US</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="i4us"])}}#tech"> I4US </a></h5><br>
                <h4 class="wow fadeInUp" data-wow-duration="2s">MPF Toolkit</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="toolkit_installation"])}}#tech"> Toolkit For Software Installation</a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="toolkit_android"])}}#tech"> Toolkit For System check(Android Only)</a></h5><br>
                <h4 class="wow fadeInUp" data-wow-duration="2s">MPF Toolkit 2.0</h4>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="app_manual"])}}#tech"> MPF Console App Manual</a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="toolkit_installation2"])}}#tech"> Toolkit 2.0 For Software Installation</a></h5>
                <h5><a class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" href="{{route("tech_series",[$series="toolkit_system_checking"])}}#tech"> Toolkit 2.0 For System Checking</a></h5>
            </div>
        </div>
    </div>
</section>
<!-- tech -->

@endsection