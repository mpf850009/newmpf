@extends('newmpf.main')
@section('content')

@include('newmpf.prod')

<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="series">
    {{-- <div class="container pt-sm-5 pt-4">
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="{{asset('images/mi9.jpg')}}" alt="mpf drive product">
        </div>
    </div> --}}
    <div class="container pt-sm-5 pt-4">
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="{{asset('images/2022 MPF6 DM-1.jpg')}}" alt="mpf drive product">
        </div>
    </div>
    <div class="container pt-sm-5 pt-4">
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="{{asset('images/2022 MPF6 DM-2.jpg')}}" alt="mpf drive product">
        </div>
    </div>
    <div class="container pt-sm-5 pt-4">
        <div class="col-md-12">
            <img class="img-responsive" width="100%" src="{{asset('images/mi11.jpg')}}" alt="mpf drive product">
        </div>
    </div>
</section>


@endsection