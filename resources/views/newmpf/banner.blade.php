@if($active=='index')
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-position: top;">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">MPF DRIVE</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">More Power Feeling!</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#team"
                            role="button">View
                            More <i class="fa fa-arrow-down"></i></a>
                    </div>
                </div>
            </div>
        </li>
        <li class="banner banner2">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        {{-- <h3>&emsp;</h3>
                        <span>&emsp;</span><br>
                        <p>&emsp;</p> --}}
                        <h3 class="wow fadeInLeft">Start your engine</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">More Power Feeling!</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr wow flip" data-blast="bgColor" href="{{route("product")}}" role="button">
                            View More <i class="fa fa-arrow-down"></i>
                        </a>
                        {{-- <a class="btn bg-theme mt-4 w3_pvt-link-bnr wow flip" data-blast="bgColor" href="{{route("news_date",[$date="20200304"])}}"
                            role="button">View
                            More <i class="fa fa-arrow-down"></i></a> --}}
                    </div>
                </div>
            </div>
        </li>
        <li class="banner banner3">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3>THE PACEMAKER</h3>
                        <span class="line"></span>
                        <p>Turns Electric Power Into Adrenalin.</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#team"
                            role="button">View
                            More <i class="fa fa-arrow-down"></i></a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='news') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/bg05.jpg'); background-position:top;">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">MPF DRIVE</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">News</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#news" role="button">View More <i class="fa fa-arrow-down"></i></a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


@elseif($active=='news1') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../../images/bg05.jpg'); background-position:top;">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">MPF DRIVE</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">News</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#news" role="button">View More <i class="fa fa-arrow-down"></i></a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='company') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/bg01.jpg'); background-position:top;">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">MPF DRIVE</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">Unique Product & Design Co., Ltd.</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#services" role="button">View More <i class="fa fa-arrow-down"></i></a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='product') 
@include('newmpf.ui')
<div class="bv">
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="../videos/video2s.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
        <div class="banner-text">
            <div class="slider-info">
                <h3 class="wow fadeInLeft">product</h3>
                <span class="line"></span>
                <p class="wow fadeInRight">high quality</p>
                <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#team" role="button">View More <i class="fa fa-arrow-down"></i></a>
            </div>
        </div>
    </div>
</div>

@elseif($active=='product1') 
@include('newmpf.ui')
<div class="bv">
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="../../videos/video2s.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
        <div class="banner-text">
            <div class="slider-info">
                <h3 class="wow fadeInLeft">product</h3>
                <span class="line"></span>
                <p class="wow fadeInRight">high quality</p>
                <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#table" role="button">View More <i class="fa fa-arrow-down"></i></a>
            </div>
        </div>
    </div>
</div>

@elseif($active=='partner') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/bg03.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">Reference</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">All New MPF Drive </p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#logo" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='contact') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/bg04.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">contact</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">Get In Touch</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#company" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='rentalmap') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/banner8.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">E-BIKE RENTAL</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">Taiwan</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#taiwan" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='rentalplace') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" @if($placename=='台南左鎮公舘社區發展協會')style="background-image: url('../../images/zuojhen/308.jpg');position:center bottom;" @else  style="background-image: url('../../images/banner8.jpg');" @endif>
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">{{$placename}}</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">MPF DRIVE電助車出租</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#place" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='series') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/banner11.jpg');background-position: center;">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">&emsp;</h3>
                        <span> &emsp; </span>
                        <p class="wow fadeInRight">&emsp;</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#series" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='tech') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../images/banner7.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">technical support</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">Get Data</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#tech" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='tech1') 
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('../../images/banner7.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">technical support</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">Get Data</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll wow flip" data-blast="bgColor" href="#tech" role="button">View More <i class="fa fa-arrow-down"></i></a>  
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

@elseif($active=='test')
@include('newmpf.ui')
<div class="bv">
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="../videos/video1.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
        <div class="banner-text">
            <div class="slider-info">
                <h3 class="wow fadeInLeft">contact</h3>
                <span class="line"></span>
                <p class="wow fadeInRight">Get In Touch</p>
            </div>
        </div>
    </div>
</div>

@else
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li class="banner banner1" style="background-image: url('images/404.jpg');">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3 class="wow fadeInLeft">404</h3>
                        <span class="line"></span>
                        <p class="wow fadeInRight">get banner</p>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
@endif