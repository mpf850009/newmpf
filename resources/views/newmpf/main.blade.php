<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zh-TW">

<head>
    <title>MPF Drive 穩正企業股份有限公司 - {{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="中置馬達,中置,馬達,電動,輔助,自行車,電動輔助自行車,MPF穩正,穩正,穩正企業,腳踏車,電動腳踏車,電動輔助腳踏車,MPF DRIVE,DRIVE,MPF,Mid Motor,Mid,Motor,mid drive motor,sentra motor,electric bicycle,ebike,e-bike,e bike,e,bike,bicycle,electric">
    
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <!-- Custom Theme files -->
    <link href="{{asset('css/bootstrap.css')}}" type="text/css" rel="stylesheet" media="all">
    <!-- hover -->
    <link href="{{asset('css/hover.css')}}" rel="stylesheet" />
    <!-- color -->
    <link href="{{asset('css/blast.min.css')}}" rel="stylesheet" />
    <!-- portfolio -->
    <link href="{{asset('css/portfolio.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('css/style1.css')}}" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!--網站圖標-->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />

    <style>
        h3 .title{
            color: #0099ff;
        }
    </style>
</head>

<body>
    <!-- banner -->
    <div id="home">

        <!-- header -->
        @include('newmpf.nav')

        @include('newmpf.banner')
    </div>
    <!-- //banner -->
    {{--  @include('newmpf.color')  --}}

    @yield('content')

    @include('newmpf.footer')
    <!-- js -->
    <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    <!-- //js -->
    <!-- script for password match -->
    {{-- <script>
        window.onload = function () {
            document.getElementById("password1").onchange = validatePassword;
            document.getElementById("password2").onchange = validatePassword;
        }

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Don't Match");
            else
                document.getElementById("password2").setCustomValidity('');
            //empty string means no validation error
        }
    </script> --}}
    <!-- script for password match -->
    <!-- Banner  Responsiveslides -->
    <script src="{{asset('js/responsiveslides.min.js')}}"></script>
    <script>
        // You can also use"$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider3").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!-- //Banner  Responsiveslides -->

    
    <!-- Scrolling Nav JavaScript -->
    <script src="{{asset('js/scrolling-nav.js')}}"></script>
    <script src="{{asset('js/counter.js')}}"></script>
    <!-- portfolio -->
    <script src="{{asset('js/jquery.picEyes.js')}}"></script>
    <script>
        $(function () {
            //picturesEyes($('.demo li'));
            $('.demo li').picEyes();
        });
    </script>
    <!-- //portfolio -->
    <!-- start-smooth-scrolling -->
    <script src="{{asset('js/move-top.js')}}"></script>
    <script src="{{asset('js/easing.js')}}"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
            };
            */
            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="{{asset('js/SmoothScroll.min.js')}}"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- color -->
    <script src="{{asset('js/blast.min.js')}}"></script>
    
   
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
    var wow = new WOW(
  {
    boxClass:     'wow',      // 要套用WOW.js縮需要的動畫class(預設是wow)
    animateClass: 'animated', // 要"動起來"的動畫(預設是animated, 因此如果你有其他動畫library要使用也可以在這裡調整)
    offset:       0,          // 距離顯示多遠開始顯示動畫 (預設是0, 因此捲動到顯示時才出現動畫)
    mobile:       true,       // 手機上是否要套用動畫 (預設是true)
    live:         true,       // 非同步產生的內容是否也要套用 (預設是true, 非常適合搭配SPA)
    callback:     function(box) {
      // 當每個要開始時, 呼叫這裡面的內容, 參數是要開始進行動畫特效的element DOM
    },
    scrollContainer: null // 可以設定成只套用在某個container中捲動才呈現, 不設定就是整個視窗
  }
);
wow.init();
</script>


{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.min.js"></script>
<script>
$(document).on("click", '[data-toggle="lightboxnew1"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>
    
</body>

</html>