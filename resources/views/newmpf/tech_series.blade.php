@extends('newmpf.main')
@section('content')

<style>
    .w100{
        width: 100%;
    }
</style>
<!-- tech -->
<section class="team py-4 py-lg-5" id="tech">
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">
                @if($series=='6C')
                    MPF6&nbsp;C
                @elseif($series=='6LT')
                    MPF6&nbsp;LT
                @elseif($series=='6S')
                    MPF6&nbsp;S
                @elseif($series=='6SL')
                    MPF6&nbsp;SL
                @elseif($series=='6ST')
                    MPF6&nbsp;ST
                @elseif($series=='648V')
                    MPF6&nbsp;(48V)
                @elseif($series=='turnonaf'||$series=='turnoni4us')
                    Easy to turn on MPF DRIVE
                @elseif($series=='5')
                    MPF5.3
                @elseif($series=='3')
                    MPF3
                @elseif($series=='toolkit_installation')
                    Toolkit For Software Installation
                @elseif($series=='app_manual')
                    MPF Console App Manual
                @elseif($series=='toolkit_installation2')
                    Toolkit 2.0 For Software Installation
                @elseif($series=='toolkit_system_checking')
                    Toolkit 2.0 For System Checking
                @elseif($series=='toolkit_android')
                    Toolkit For System check (Android Only)
                @elseif($series=='i4us')
                    Display I4US
                @elseif($series=='i4u')
                    Display I4U
                @elseif($series=='af')
                    Display AF
                @else
                    MPF X Series
                @endif
            </h3>
        </div>
        
        <div class="container text-left pt-sm-5 pt-4">
            @if($series=='6C'||$series=='6S'||$series=='6SL')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+AF-20190618pdf.pdf')}}"> MPF6 + AF -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6+AF-20190618pdf.jpg')}}" class="img-responsive w100" alt=""><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+I4U-20190618.pdf')}}"> MPF6 + I4U -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6+I4U-20190618_p001.jpg')}}" class="img-responsive w100" alt=""><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+I4US-20191108.pdf')}}"> MPF6 + I4US -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6+I4US-20191108_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                @if($series=='6C')
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+Xplova(EH-02)-standard-20211013.pdf')}}"> MPF6 + Xplova(EH-02) -> PDF Download </a></h5>
                    <img src="{{asset('files/cableplan/MPF6+Xplova(EH-02)-standard-20211013_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                @endif
                @if($series=='6S'||$series=='6SL')
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6(H-plug)+I4US-20211013.pdf')}}"> MPF6(H-plug) + I4US -> PDF Download </a></h5>
                    <img src="{{asset('files/cableplan/MPF6(H-plug)+I4US-20211013_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                @endif
                <br><br>

                @if($series=='6C')
                    <h4 class="wow fadeInUp">Chain line</h4>
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/chainline/MPF6C/standard chainline 6.0 style-20160412.pdf')}}"> standard chainline 6.0 style-20160412 -> PDF Download </a></h5>
                    <img src="{{asset('files/chainline/MPF6C/0001.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6C/0002.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6C/0003.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6C/0004.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6C/0005.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                    
                    <h4 class="wow fadeInUp">Dimension</h4>
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/dimension/MPF 6C.pdf')}}"> MPF6 C -> PDF Download </a></h5>
                    <img src="{{asset('files/dimension/MPF 6C.jpg')}}" class="img-responsive w100" alt=""><br>

                @elseif($series=='6S')
                    <h4 class="wow fadeInUp">Chain line</h4>
                    <img src="{{asset('files/chainline/MPF6S/BCEISIS002 38T chainwheel 6.0 ISIS style.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6S/BCEISIS003 32T chainwheel 6.0 ISIS style.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6S/BCEISIS011 standard chainline 6.0 ISIS style-chain system for single chainwheel-20170908.jpg')}}" class="img-responsive w100" alt=""><br>
                    <img src="{{asset('files/chainline/MPF6S/BCEISIS015 single spider belt system PCD104 CL 54.7-20180322 (E-lom).jpg')}}" class="img-responsive w100" alt=""><br><br><br>

                    <h4 class="wow fadeInUp">Dimension</h4>
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/dimension/MFP 6S(135L).pdf')}}"> MPF6 S 135L -> PDF Download </a></h5>
                    <img src="{{asset('files/dimension/MFP 6S(135L).jpg')}}" class="img-responsive w100" alt=""><br>
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/dimension/MPF 6S (137L).pdf')}}"> MPF6 S 137L -> PDF Download </a></h5>
                    <img src="{{asset('files/dimension/MPF 6S (137L).jpg')}}" class="img-responsive w100" alt=""><br>

                @elseif($series=='6SL')
                    <h4 class="wow fadeInUp">Chain line</h4>
                    <img src="{{asset('files/chainline/MPF6SL/BCEISIS016 fatbike chainline 6.0 ISIS style-chain system for single chain wheel-20181116 (KHS).jpg')}}" class="img-responsive w100" alt=""><br><br><br>

                    <h4 class="wow fadeInUp">Dimension</h4>
                    <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/dimension/MPF 6SL (155L).pdf')}}"> MPF6 SL (155L) -> PDF Download </a></h5>
                    <img src="{{asset('files/dimension/MPF 6SL (155L).jpg')}}" class="img-responsive w100" alt=""><br>
                    
                @endif

            @elseif($series=='6ST')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6ST+I4US Throttle-20191004.pdf')}}"> MPF6 ST Series -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6ST+I4US Throttle-20191004_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6(H-plug)+I4US Throttle-20211013.pdf')}}"> MPF6(H-plug) + I4US Throttle -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6(H-plug)+I4US Throttle-20211013_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>

            @elseif($series=='6LT')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6(H-plug)+I4US-20200918.pdf')}}"> MPF6 LT Series -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6(H-plug)+I4US-20200918_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+Xplova(EH-02)-standard-20211013.pdf')}}"> MPF6 + Xplova(EH-02) -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6+Xplova(EH-02)-standard-20211013_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
            
            @elseif($series=='648V')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF6+I4US 48V-20211013.pdf')}}"> MPF6 48V Series -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF6+I4US 48V-20211013_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
            
            @elseif($series=='turnonaf')
                <div class="text-center">
                    <h4 class="wow fadeInUp">AF</h4><br>
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/i8tmPLvrdJU?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                    </iframe>
                </div><br>
            
            @elseif($series=='turnoni4us')
                <div class="text-center">
                    <h4 class="wow fadeInUp">I4US</h4><br>
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/W1Xnn16D3VU?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                    </iframe>
                </div><br>

            @elseif($series=='5')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF5.3+AF-20190618-2.pdf')}}"> MPF5.3 Series + AF -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF5.3+AF-20190618-2_page-0001.jpg')}}" class="img-responsive w100" alt=""><br><br><br>

                <h4 class="wow fadeInUp">Chain line</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/chainline/MPF5.3/Gates Carbon Drive CenterTrack with 7.5T spider(5.3 style)-20141203.pdf')}}"> Gates Carbon Drive CenterTrack with 7.5T spider(5.3 style)-20141203 -> PDF Download </a></h5>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/chainline/MPF5.3/standard chainline MPF5.3 style-2019.pdf')}}"> standard chainline MPF5.3 style-2019 -> PDF Download </a></h5>
                <img src="{{asset('files/chainline/MPF5.3/MPF5.3 32T.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/chainline/MPF5.3/MPF5.3 38T.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/chainline/MPF5.3/MPF5.3 belt.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/chainline/MPF5.3/MPF5.3 single.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/chainline/MPF5.3/MPF5.3 triple.jpg')}}" class="img-responsive w100" alt=""><br><br><br>

                <h4 class="wow fadeInUp">Dimension</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/dimension/MPF5.3.pdf')}}"> MPF5.3 -> PDF Download </a></h5>
                <img src="{{asset('files/dimension/MPF5.3.jpg')}}" class="img-responsive w100" alt=""><br>
            @elseif($series=='3')
                <h4 class="wow fadeInUp">Cable plan</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/cableplan/MPF3-20190618.pdf')}}"> MPF3 Series -> PDF Download </a></h5>
                <img src="{{asset('files/cableplan/MPF3-20190618_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>

                <h4 class="wow fadeInUp">Chain line</h4>
                <img src="{{asset('files/chainline/MPF3/BCE30101 38T chainwheel 3.0 ISIS.jpg')}}" class="img-responsive w100" alt=""><br>
            @elseif($series=='toolkit_installation')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation.pdf')}}"> MPF TOOL KIT MANUAL-Android  Software Installation -> PDF Download </a></h5><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0004.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0005.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android  Software installation_page-0006.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                {{-- <h4 class="wow fadeInUp">Video Introduction</h4><br> --}}
            @elseif($series=='app_manual')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/MPF Console app manual-20211019.pdf')}}"> MPF Console app manual-20211019 -> PDF Download </a></h5><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0004.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0005.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0006.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0007.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0008.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0009.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0010.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0011.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0012.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0013.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0014.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Console app manual-20211019_page-0015.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
            @elseif($series=='toolkit_installation2')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923.pdf')}}"> MPF TOOLKIT 2.0 Manual Software Installation -> PDF Download </a></h5><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0004.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0005.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0006.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0007.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0008.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0009.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0010.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0011.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_Software installation-20210923_page-0012.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
            @elseif($series=='toolkit_system_checking')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923.pdf')}}"> MPF TOOLKIT 2.0 Manual System Checking -> PDF Download </a></h5><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0004.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0005.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0006.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0007.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0008.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0009.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0010.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0011.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0012.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0013.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF Toolkit 2 manual_System checking-20210923_page-0014.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
            @elseif($series=='toolkit_android')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking.pdf')}}"> MPF TOOL KIT MANUAL-Android-System checking -> PDF Download </a></h5><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0004.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0005.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0006.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/MPF TOOL KIT MANUAL-Android-System checking_page-0007.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                <h4 class="wow fadeInUp">Video Introduction</h4>
                <iframe class="contact-form-wthreelayouts wow jackInTheBox w100" src="https://www.youtube.com/embed/Kali89FNvao" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="height:500px"></iframe>
                <br>
            @elseif($series=='i4us')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/I4US USER MANUAL.pdf')}}"> I4US USER MANUAL -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/I4US USER MANUAL_page-0001.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/Padel sensitivity setting-I4US-20190516.pdf')}}"> Padel sensitivity setting-I4US-20190516 -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/Padel sensitivity setting-I4US-20190516_page-0001.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/Bike information code-I4US-20190516.pdf')}}"> Bike information code-I4US-20190516 -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/Bike information code-I4US-20190516_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/Bike information code-I4US-20190516_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/Bike information code-I4US-20190516_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
            @elseif($series=='i4u')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/I4U USER MANUAL.pdf')}}"> I4U USER MANUAL -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/I4U USER MANUAL.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/Padel sensitivity setting-20170822.pdf')}}"> Padel sensitivity setting-20170822 -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/Padel sensitivity setting-20170822_page-0001.jpg')}}" class="img-responsive w100" alt=""><br><br><br>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/Bike information code-20170822.pdf')}}"> Bike information code-20170822 -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/Bike information code-20170822_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/Bike information code-20170822_page-0002.jpg')}}" class="img-responsive w100" alt=""><br>
                <img src="{{asset('files/user manual/Bike information code-20170822_page-0003.jpg')}}" class="img-responsive w100" alt=""><br>
            @elseif($series=='af')
                <h4 class="wow fadeInUp">User Manual</h4>
                <h5><a download onclick="return confirm('確認下載檔案？');" class="wow fadeInUp" data-wow-duration="2s" href="{{asset('files/user manual/AF USER MANUAL.pdf')}}"> AF USER MANUAL -> PDF Download </a></h5>
                <img src="{{asset('files/user manual/AF USER MANUAL_page-0001.jpg')}}" class="img-responsive w100" alt=""><br>
                <br><br>
            @else
                <h4>select series</h4>
            @endif
        </div>
    </div>
</section>
<!-- tech -->

@endsection