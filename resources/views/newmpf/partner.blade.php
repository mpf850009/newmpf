@extends('newmpf.main')
@section('content')

<style>
    .pad0{
        padding: 0px 0px;
    }
    .pad10{
        padding:8px 8px;
    }
    .vv{
        width: 100%;
        padding:0px 0px;
        border-color:#0099ff;
        border-width:3px;
        border-radius: 30px;
    }
</style>

<!-- logo -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="logo">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">brand</h3>
        </div>
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl6">
                        <img src="{{asset('images/pl6.jpg')}}" alt="mpf drive igo bike" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl1">
                        <img src="{{asset('images/pl1.jpg')}}" alt="mpf drive manta5 partner" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl2">
                        <img src="{{asset('images/pl2.jpg')}}" alt="mpf drive ceclo" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl3">
                        <img src="{{asset('images/pl3.jpg')}}" alt="mpf drive pelican-bike" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl4">
                        <img src="{{asset('images/pl4.jpg')}}" alt="mpf drive igo bike" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl5">
                        <img src="{{asset('images/pl5.jpg')}}" alt="mpf drive igo bike" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl7">
                        <img src="{{asset('images/pl7.jpg')}}" alt="mpf drive igo bike" class="vv img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl8">
                        <img src="{{asset('images/pl8.png')}}" alt="mpf drive igo bike" class="vv img-fluid img-thumbnail" style="background-color: black" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl9.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl10.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl11.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl12.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl13.png')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl15.png')}}" alt="mpf drive" class="vv img-fluid img-thumbnail" style="background-color: black"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl16.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl17.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl18.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl19.png')}}" alt="mpf drive" class="vv img-fluid img-thumbnail" style="background-color: black"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl20.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl21.png')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl22.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl23.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl24.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl25.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl26.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#">
                        <img src="{{asset('images/pl27.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 col-6 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a class="scroll" href="#pl28">
                        <img src="{{asset('images/pl28.jpg')}}" alt="mpf drive" class="vv img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- logo -->

<!-- partner -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="1">
    <div class="container-fluid">
        <div class="title-desc text-center pb-sm-3"><br>
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">Reference</h3>
            <p></p>
        </div>
        <ul class="demo row py-lg-5 py-sm-4 pb-4">
            <li id="pl6" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/Zue2qP8a9AI?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl1" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/K49ohhaXtgo?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl2" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/nQszTK8mcSM?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl3" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/YcOkrUaujl4?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl4" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/K9MLQ7etuM8?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl5" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/DY5PSAKml50?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl7" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/riKgy2-J7qU?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl8" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/CDUyRxTcxcA?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
            <li id="pl28" class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/G4EwEUfjX-s?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0"
                    allowfullscreen>
                </iframe>
            </li>
        </ul>
    </div>
</section>
<!-- partner -->

<!-- presentation -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="2">
    <div class="container-fluid">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">Presentation</h3>
            <p></p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/YXKk31SCX0Q?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <video width="100%" height="400" muted="muted" loop="loop" controls>
                    <source src="{{asset('videos/video2s.mp4')}}" type="video/mp4">
                </video>
            </div>
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/xSZKc9fko4w?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                </iframe>
            </div>
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/A7W6MUDvc8g?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                </iframe>
            </div>
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/QoIy_npMA4Y?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                </iframe>
            </div>
            <div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/2zE-Q9K8Dwk?autoplay=1&rel=1&showinfo=0&loop=1" frameborder="0" allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
</section>
<!-- presentation -->
@endsection