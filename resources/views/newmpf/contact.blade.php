@extends('newmpf.main')
@section('content')

<style>
    .pad0{
        padding: 0px 0px;
    }
    .pad10{
        padding:8px 8px;
    }
    .vv{
        width: 100%;
        padding:15px 15px;
        border-color:#0099ff;
        border-width:3px;
        border-radius: 30px;
    }
</style>
<!-- company -->
<div class="w3lspvt-about py-md-5 py-5" id="company">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">穩正企業股份有限公司</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">Unique Product & Design Co., Ltd.</p>
        </div>
        <div class="w3lspvt-about-row row text-center pt-md-0 pt-5 mt-lg-5">
            <div class="col-sm-6 w3lspvt-about-grids">
                <div class="p-md-5 p-sm-3">
                    <span class="fa fa-map-marker wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-blast="color" data-wow-duration="2s">Location</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">成立於1989年，公司位於台灣台南市永康區，是一家助動自行車中置馬達的專業研發設計製造公司。</p>
                </div>
            </div>
            <div class="col-sm-6 w3lspvt-about-grids text-center border-left my-sm-0 my-5">
                <div class="p-md-5 p-sm-3">
                    <span class="fa fa-check-circle-o wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-blast="color" data-wow-duration="2s">Quality</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">從產品的設計、研發到生產都是一貫作業，並採用高科技的生產設備來進行產品的生產與製造，以維持產品的高品質。</p>
                </div>
            </div>
        </div>
        <div class="w3lspvt-about-row border-top row text-center pt-md-0 pt-5 mt-md-0 mt-5">
            <div class="col-sm-6 w3lspvt-about-grids">
                <div class="p-md-5 p-sm-3 col-label">
                    <span class="fa fa-paw wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-wow-duration="2s" data-blast="color">reliable</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">「立足台灣，佈局全球」是穩正企業長期的發展策略。為確保台灣的技術優勢成為研發設計與生產高附加價值產品的據點。 積極建構以台灣為主軸的研發中心，整合美洲、歐洲和亞洲等各知名品牌車廠，成就全球研發、製造與銷售的強勢競爭力。</p>
                </div>
            </div>
            <div class="col-sm-6 w3lspvt-about-grids mt-lg-0 mt-md-3 border-left pt-sm-0 pt-5">
                <div class="p-md-5 p-sm-3 col-label">
                    <span class="fa fa-handshake-o wow flip" data-wow-duration="2s" data-blast="borderColor">
                    </span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-wow-duration="2s" data-blast="color">partner</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">長期以來，我們憑藉價值行銷、研究創新、注重品質與客戶服務於市場上，不斷提昇競爭力並取得眾多客戶之信任，進而創造公司與客戶雙贏局面，公司得以不斷成長！</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- company -->

<!-- portfolio -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="portfolio">
    <div class="container-fluid">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">photo</h3>
            <p class="wow fadeInUp" data-wow-duration="2s"></p>
        </div>
        <ul class="demo row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/c2.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/banner1.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6 mx-auto">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/c4.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/pic4.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/pic5.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/pic6.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad10" data-wow-duration="2s">
                    <img src="{{asset('images/pic7.jpg')}}" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- portfolio -->

<!-- contact -->
<section class="contact-wthree py-sm-5 py-4" id="contact">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">contact us</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">Experience & improve on bike issues.</p>
            <h5 class="cont-form wow fadeInUp" data-wow-duration="2s" data-blast="color">get in touch</h5>
        </div>
        <div class="row mt-2">
            <div class="col-lg-3 text-center">
                <div class="row flex-column">
                    <div class="contact-w3">
                        <a href="mailto:ec@mpf.tw">
                            <span class="fa fa-envelope-open mb-3 wow flip" data-blast="color"></span>
                        </a>
                        <div class="d-flex flex-column">
                            <p class="wow fadeInUp" data-wow-duration="2s">
                                資深副總<br>Senior Vice General Manager<br>
                                陳鵬旭 Eddy Chen <br>
                                Mobile: + 886-910-106427 <br>
                                E-Mail: <a href="mailto:ec@mpf.tw">ec@mpf.tw</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 text-center">
                <div class="row flex-column">
                    <div class="contact-w3">
                        <a href="mailto:eric.hsieh@mpf.tw">
                            <span class="fa fa-envelope-open mb-3 wow flip" data-blast="color"></span>
                        </a>
                        <div class="d-flex flex-column">
                            <p class="wow fadeInUp" data-wow-duration="2s">
                                業務行銷副總<br>Sales Vice General Manager<br>
                                謝宗達 Eric Hsieh<br>
                                Mobile: + 886-933-200618 <br>
                                E-Mail: <a href="mailto:eric.hsieh@mpf.tw">eric.hsieh@mpf.tw</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 text-center">
                <div class="row flex-column">
                    <div class="contact-w3">
                        <span class="fa fa-phone mb-3 wow flip" data-blast="color"></span>
                        <div class="d-flex flex-column wow fadeInUp" data-wow-duration="2s">
                            <p>TEL : +886-6-253-8390</p>
                            <p>FAX : +886-6-254-1507</p>
                            <p>E-Mail : <a href="mailto:sales@mpf.tw">sales@mpf.tw</a></p>
                            <p>　</p><p>　</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 text-center">
                <div class="row flex-column">
                    <div class="contact-w3">
                        <span class="fa fa-home mb-3 wow flip" data-blast="color"></span>
                        <div class="d-flex flex-column">
                            <p class="wow fadeInUp" data-wow-duration="2s">Unique Product & Design Co., Ltd.<br>穩正企業股份有限公司<br>No. 5, Ming Dong Road, Yong Kang District,Tainan,Taiwan(ROC). <br>台南市永康區民東路5號</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="mx-auto text-center mt-lg-0 mt-4">
            <iframe class="mt-lg-4 contact-form-wthreelayouts wow jackInTheBox" data-wow-duration="2s" data-blast="borderColor" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14684.78059868254!2d120.259536!3d23.053306!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4c4704ff65de7975!2z56mp5q2j5LyB5qWt6IKh5Lu95pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1545378575700"
                allowfullscreen></iframe>
            <!-- footer right -->
        </div>
    </div>
</section>

<!-- contact -->

@endsection