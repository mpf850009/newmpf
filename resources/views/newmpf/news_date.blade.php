@extends('newmpf.main')
@section('content')

<!-- news -->
@if($date=='20190312')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">台北國際自行車展覽會</h3>
            <p style="font-size:22px" class="wow fadeInUp" data-wow-duration="2s">2019 03.27-30</p>
            <p style="font-size:22px" class="wow fadeInUp" data-wow-duration="2s">南港展覽館 #N0718</p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <a class="wow fadeInUp" data-wow-duration="2s" href="#">
                    <img class="img-responsive" width="100%" src="{{asset('images/news1.jpg')}}" alt="台北國際自行車展覽會 南港展覽館 穩正">
                </a><br>
            </div>
        </div>
    </div>
</section>

@elseif($date=='20190301')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">All New MPF3 Series</h3>
            <p style="font-size:22px" class="wow fadeInUp" data-wow-duration="2s">Lighter & Smaller</p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <a class="wow fadeInUp" data-wow-duration="2s" href="#">
                    <img class="img-responsive" width="100%" src="{{asset('images/banner5.jpg')}}" alt="All New MPF3 Series">
                </a><br>
            </div>
        </div>
    </div>
</section>

@elseif($date=='20190318')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF6系列通過EN15194(13849)/2017認證</h3>
            <p class="wow fadeInUp" data-wow-duration="2s"></p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/en15194.jpg')}}" alt="MPF6系列通過EN15194(13849)/2017認證"><br>
                <img class="img-responsive" width="100%" src="{{asset('images/news2.jpg')}}" alt="">
                <img class="img-responsive" width="100%" src="{{asset('images/news3.jpg')}}" alt="">
                <img class="img-responsive" width="100%" src="{{asset('images/news4.jpg')}}" alt="">
            </div>
        </div>
    </div>
</section>

@elseif($date=='20190408')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s"> 感謝台北展期間的雜誌報導 </h3>
            <p class="wow fadeInUp" data-wow-duration="2s"></p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/news5.jpg')}}" alt="感謝台北展期間的雜誌報導"><br>
                <img class="img-responsive" width="100%" src="{{asset('images/news6.jpg')}}" alt="感謝台北展期間的雜誌報導">
            </div>
            <div class="text-right">
                <br><a target="_blank" class="btn bg-theme mt-3 w3_pvt-link-bnr wow fadeInUp" data-wow-duration="2s" data-blast="bgColor" href="https://www.bike-eu.com/home/nieuws/2019/03/e-bike-puts-taiwans-export-back-on-track-10135600" role="button">相關連結 <span class="fa fa-search"></span></a>
            </div>
        </div>
    </div>
</section>

@elseif($date=='20191016')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s"> 台中週自行車 </h3>
            <p style="font-size:22px" class="wow fadeInUp" data-wow-duration="2s">@Booth#1363&1364, 13F<br>台中金典酒店 The Splendor Hotel, Taichung</p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/news7.jpg')}}" alt="台中週自行車 穩正 mpf drive Taichung Bike Week">
            </div>
            <div class="text-right">
                <br><a target="_blank" class="btn bg-theme mt-3 w3_pvt-link-bnr wow fadeInUp" data-wow-duration="2s" data-blast="bgColor" href="http://www.taichungbikeweek.com" role="button">相關連結 <span class="fa fa-search"></span></a>
            </div>
        </div>
    </div>
</section>

@elseif($date=='20200304')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt" data-wow-duration="2s">台北國際自行車展覽會</h3>
            <p style="font-size:22px" data-wow-duration="2s">2020 03.04-07</p>
            <p style="font-size:22px" data-wow-duration="2s">南港展覽館2館 #R0520</p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <a data-wow-duration="2s">
                    <img class="img-responsive" width="100%" src="{{asset('images/news8.jpg')}}" alt="台北國際自行車展覽會 南港展覽館 2館 穩正 自行車展覽會 自行車 Taipei Cycle">
                </a><br>
            </div>
        </div>
        <div class="text-right">
            <br><a target="_blank" class="btn bg-theme mt-3 w3_pvt-link-bnr" data-wow-duration="2s" data-blast="bgColor" href="https://www.taipeicycle.com.tw/zh_TW/index.html" role="button">Taipei Cycle&nbsp;<span class="fa fa-search"></span></a>
        </div>
    </div>
</section>

@elseif($date=='20220309')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt" data-wow-duration="2s">台北國際自行車展覽會</h3>
            <p style="font-size:22px" data-wow-duration="2s">2022 03.09-12</p>
            <p style="font-size:22px" data-wow-duration="2s">南港展覽館1館4F #N0414</p>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <a data-wow-duration="2s">
                    <img class="img-responsive" width="100%" src="{{asset('images/news10.jpg')}}" alt="台北國際自行車展覽會 南港展覽館 1館 穩正 自行車展覽會 自行車 Taipei Cycle">
                </a><br>
            </div>
        </div>
        <div class="text-right">
            <br><a target="_blank" class="btn bg-theme mt-3 w3_pvt-link-bnr" data-wow-duration="2s" data-blast="bgColor" href="https://www.taipeicycle.com.tw/zh_TW/index.html" role="button">Taipei Cycle&nbsp;<span class="fa fa-search"></span></a>
        </div>
    </div>
</section>
@endif

<!-- news -->
@endsection