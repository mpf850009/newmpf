<header>
    {{-- css 底色黑色 --}}
    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-fixed-top">
        <a class="navbar-brand" href="{{route('index')}}" data-blast="color">
            <img src="{{asset('images/mpfdrive3.png')}}" class="img-fluid" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-lg-auto text-center">                
                <li @if($active=='index')class="nav-item active"@endif class="nav-item" >
                    <a class="nav-link" href="{{route('index')}}">Home</a>
                </li>
                <li @if($active=='news'||$active=='news1')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('news')}}">news</a>
                </li>
                {{--  <li @if($active=='company')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('company')}}">company</a>
                </li>  --}}
                <li @if($active=='product'||$active=='product1')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('product')}}">product</a>
                </li>
                <li @if($active=='contact')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('contact')}}">contact</a>
                </li>
                <li @if($active=='partner')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('partner')}}">Reference</a>
                </li>
                <li @if($active=='tech'||$active=='tech1')class="nav-item active"@endif class="nav-item  mt-lg-0 mt-3">
                    <a class="nav-link" href="{{route('tech')}}">technical support</a>
                </li>
                <li class="nav-item  mt-lg-0 mt-3">
                    <a target="_blank" class="nav-link" href="https://www.mpfinside.com.tw/service">Customer Service</a>
                </li>
                <li @if($active=='rentalmap'||$active=='rentalplace'||$active=='series')class="nav-item dropdown active"@endif class="nav-item dropdown mt-lg-0 mt-3">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        mpfinside®
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        {{--  <a class="dropdown-item nav-link" href="/userapp">User App</a>  --}}
                        <a class="dropdown-item nav-link" href="{{route('series')}}">mpfinside®</a> 
                        <a class="dropdown-item nav-link" href="{{route('rentalmap')}}">e-bike rental</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>