<style>
.bv {
    position: relative;
    background-color: black;
    height: 100vh;
    width: 100%;
    overflow: hidden;
}

.bv video {
    position: absolute;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: 0;
    -ms-transform: translateX(-50%) translateY(-50%);
    -moz-transform: translateX(-50%) translateY(-50%);
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
}

.bv .container {
    position: relative;
    z-index: 2;
}

.bv .overlay {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background-color: black;
    opacity: 0.5;
    z-index: 1;
}

@media (pointer: coarse) and (hover: none) {
    .bv {
        background: url('../public/images/banner3.jpg') black no-repeat center left;
        height: 500px;
    }
    .bv video {
        display: none;
    }
}
</style>