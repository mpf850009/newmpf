@extends('newmpf.main')
@section('content')
<style>
    .row .box13{
        padding:15px 15px;
    }
    .ww{
        width: 100%;
        padding:0 0;
        border-color:#0099ff;
        border-width:3px;
        border-radius: 30px;
    }
</style>

@if($series=='3')
<section class="contact-wthree py-sm-5 py-4" id="table">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt">product details</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5 class="cont-form" data-blast="color">MPF 3</h5>
                <div class="contact-form-wthreelayouts">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">MPF3 Series</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Model Type</th>
                                <td>MPF3</td>
                            </tr>
                            <tr>
                                <th scope="row">Application</th>
                                <td>
                                    Folding Bike<br>
                                    Mini Velo<br>
                                    Traditional Bike
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Power</th>
                                <td>250</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. support level</th>
                                <td>200%</td>
                            </tr>
                            <tr>
                                <th scope="row">Backpedal</th>
                                <td>No</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. Torque</th>
                                <td>40</td>
                            </tr>
                            <tr>
                                <th scope="row">Speed</th>
                                <td>25km/h</td>
                            </tr>
                            <tr>
                                <th scope="row">Axis Type</th>
                                <td>ISIS</td>
                            </tr>
                            <tr>
                                <th scope="row">IP Rating</th>
                                <td>65</td>
                            </tr>
                            <tr>
                                <th scope="row">Throttle</th>
                                <td>No</td>
                            </tr>
                            <tr>
                                <th scope="row">Weight</th>
                                <td>2.9kg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- table -->

<!-- product3 news-->
<section class="py-lg-5 py-4">
    <div class="container py-md-5">
        <div class="row">
            <div class="col-lg-12">
                <img src="{{asset('images/banner5.jpg')}}" alt=" " class="img-fluid" />
            </div>
            
        </div>
    </div>
</section>
<!-- product3 news -->

<!-- product3 -->
<section class="py-lg-5 py-4" id="product3">
    <div id="mpf3" class="container py-md-5">
        <div class="title text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF Drive 3</h3>
        </div>
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6"></li>
            <li class="col-lg-4 col-sm-6">
                <div class="wow fadeInUp" data-wow-duration="2s">
                    <a href="{{asset('images/m3.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m3.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                    {{-- <img id="myImg" src="{{asset('images/m3.jpg')}}" alt=" " class="" width="100%" height="auto" />
                    <div class="box-content">
                        <h3 class="title" data-blast="Color">MPF Drive 3</h3>
                    </div> --}}
                </div>
            </li>
            <li class="col-lg-4 col-sm-6"></li>
        </ul>
    </div>
</section>
<!-- product3 -->

@elseif($series=='5')
<section class="contact-wthree py-sm-5 py-4" id="table">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt">product details</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5 class="cont-form" data-blast="color">MPF 5</h5>
                <div class="contact-form-wthreelayouts">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">MPF5 Series</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Model Type</th>
                                <td>MPF5.3</td>
                                <td>MPF5.3 Cargo</td>
                            </tr>
                            <tr>
                                <th scope="row">Application</th>
                                <td>
                                    City Bike<br>
                                    Trekking Bike<br>
                                    Road Bike
                                </td>
                                <td>
                                    E-MTB<br>
                                    Off-Road<br>
                                    Cargo
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Power</th>
                                <td>250</td>
                                <td>250</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. support level</th>
                                <td>300%</td>
                                <td>400%</td>
                            </tr>
                            <tr>
                                <th scope="row">Backpedal</th>
                                <td>Optional</td>
                                <td>No</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. Torque</th>
                                <td>60</td>
                                <td>75</td>
                            </tr>
                            <tr>
                                <th scope="row">Speed</th>
                                <td>25km/h<br>32km/h</td>
                                <td>17km/h</td>
                            </tr>
                            <tr>
                                <th scope="row">Axis Type</th>
                                <td>JIS</td>
                                <td>JIS</td>
                            </tr>
                            <tr>
                                <th scope="row">IP Rating</th>
                                <td>67</td>
                                <td>67</td>
                            </tr>
                            <tr>
                                <th scope="row">Throttle</th>
                                <td>Optional</td>
                                <td>Optional</td>
                            </tr>
                            <tr>
                                <th scope="row">Weight</th>
                                <td>5.1kg</td>
                                <td>5.1kg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- table -->

<!-- product5 -->
<section class="py-lg-5 py-4" id="product2">
    <div id="mpf5" class="container py-md-5">
        <div class="title text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF Drive 5</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">CAB Bus communication technology RCW-Technology<br>(Rapid Chain Wheel Technology - chain wheel moves faster than the cranks)</p>
        </div>
        
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6"></li>
            <li class="col-lg-4 col-sm-6">
                <div class="wow fadeInUp" data-wow-duration="2s">
                    <a href="{{asset('images/m5.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m5.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                    {{-- <img src="{{asset('images/m5.jpg')}}" alt=" " class="img-fluid img-thumbnail" />
                    <div class="box-content">
                        <h3 class="title" data-blast="Color">MPF Drive 5</h3>
                    </div> --}}
                </div>
            </li>
            <li class="col-lg-4 col-sm-6"></li>
        </ul>
    </div>
</section>
<!-- product5 -->

@elseif($series=='6')
<section class="contact-wthree py-sm-5 py-4" id="table">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt">product details</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5 class="cont-form" data-blast="color">MPF 6</h5>
                <div class="contact-form-wthreelayouts">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" colspan="7" style="text-align:center;">MPF6 Series</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Model Type</th>
                                <td>MPF6LT</td>
                                <td>MPF6C</td>
                                <td>MPF6S</td>
                                <td>MPF6SL</td>
                                <td>MPF6ST</td>
                                <td>MPF6S Cargo</td>
                            </tr>
                            <tr>
                                <th scope="row">Application</th>
                                <td>
                                    City Bike<br>
                                    Trekking Bike<br>
                                    Road Bike<br>
                                    Public Bike
                                </td>
                                <td>
                                    City Bike<br>
                                    Boat<br>
                                    Water Bike
                                </td>
                                <td>
                                    E-MTB<br>
                                    Off-Road<br>
                                    Cargo
                                </td>
                                <td>
                                    Fatbike
                                </td>
                                <td>
                                    E-MTB<br>
                                    Off-Road<br>
                                    Cargo
                                </td>
                                <td>
                                    Boat<br>
                                    Cargo<br>
                                    Tricycle
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Power</th>
                                <td>250</td>
                                <td>250</td>
                                <td>250</td>
                                <td>250</td>
                                <td>250</td>
                                <td>250</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. support level</th>
                                <td>300%</td>
                                <td>350%</td>
                                <td>400%</td>
                                <td>400%</td>
                                <td>400%</td>
                                <td>450%</td>
                            </tr>
                            <tr>
                                <th scope="row">Backpedal</th>
                                <td>Optional</td>
                                <td>Optional</td>
                                <td>No</td>
                                <td>No</td>
                                <td>No</td>
                                <td>Optional</td>
                            </tr>
                            <tr>
                                <th scope="row">Max. Torque(Nm)</th>
                                <td>60</td>
                                <td>70</td>
                                <td>80</td>
                                <td>80</td>
                                <td>80</td>
                                <td>100</td>
                            </tr>
                            <tr>
                                <th scope="row">Speed</th>
                                <td>25km/h<br>32km/h</td>
                                <td>25km/h<br>32km/h<br>45km/h</td>
                                <td>45km/h</td>
                                <td>45km/h</td>
                                <td>25km/h<br>32km/h</td>
                                <td>17km/h</td>
                            </tr>
                            <tr>
                                <th scope="row">Axis Type</th>
                                <td>JIS</td>
                                <td>JIS</td>
                                <td>ISIS</td>
                                <td>ISIS</td>
                                <td>ISIS</td>
                                <td>ISIS</td>
                            </tr>
                            <tr>
                                <th scope="row">IP Rating</th>
                                <td>67</td>
                                <td>67</td>
                                <td>67</td>
                                <td>67</td>
                                <td>67</td>
                                <td>67</td>
                            </tr>
                            <tr>
                                <th scope="row">Throttle</th>
                                <td>No</td>
                                <td>No</td>
                                <td>Optional</td>
                                <td>Optional</td>
                                <td>Yes</td>
                                <td>Optional</td>
                            </tr>
                            <tr>
                                <th scope="row">Weight</th>
                                <td>4.2kg</td>
                                <td>4.7kg</td>
                                <td>4.7kg</td>
                                <td>4.7kg</td>
                                <td>4.7kg</td>
                                <td>4.7kg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- table -->


<!-- product6 -->
<section class="py-lg-5 py-4" id="product">
    <div class="container py-md-5">
        <div class="text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF Drive 6</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">MPF Drive motor generation MPF6 supports with increased torque improved peak power lowered weight and upgraded efficiency. Of course in the robust design with the full integrated metal gear system definitely oil lubricated.</p>
        </div>

        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-3 col-sm-6" style="text-align:center;">
                <h4>MPF 6C</h4>
                <div class="wow bounceInLeft" data-wow-duration="2s">
                    <a href="{{asset('images/m6c.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m6c.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                    {{-- <img src="{{asset('images/m6c.jpg')}}" alt=" " class="img-fluid img-thumbnail" />
                    <div class="box-content">
                        <h3 class="title" data-blast="Color">MPF Drive 6C</h3>
                    </div> --}}
                </div>
            </li>
            <li class="col-lg-3 col-sm-6" style="text-align:center;">
                <h4>MPF 6LT</h4>
                <div class="wow bounceInDown" data-wow-duration="2s">
                    <a href="{{asset('images/m6lt.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m6lt.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                    {{-- <img src="{{asset('images/m6lt.jpg')}}" alt=" " class="img-fluid img-thumbnail" />
                    <div class="box-content">
                        <h3 class="title" data-blast="Color">MPF Drive 6LT</h3>
                    </div> --}}
                </div>
            </li>
            <li class="col-lg-3 col-sm-6" style="text-align:center;">
                <h4>MPF 6S</h4>
                <div class="wow bounceInLeft" data-wow-duration="2s">
                    <a href="{{asset('images/m6s.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m6s.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                </div>
            </li>
            <li class="col-lg-3 col-sm-6" style="text-align:center;">
                <h4>MPF 6SL</h4>
                <div class="wow bounceInUp" data-wow-duration="2s">
                    <a href="{{asset('images/m6sl.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/m6sl.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                    {{-- <img src="{{asset('images/m6sl.jpg')}}" alt=" " class="img-fluid img-thumbnail" />
                    <div class="box-content">
                        <h3 class="title" data-blast="Color">MPF Drive 6SL</h3>
                    </div> --}}
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- product6 -->

@elseif($series=='af')
<section class="contact-wthree py-sm-5 py-4" id="table">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt">product details</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5 class="cont-form" data-blast="color">AF</h5>
                <div class="contact-form-wthreelayouts">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">AF Series</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Motor</th>
                                <td>
                                    ON/OFF
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Support level</th>
                                <td>
                                    10 steps
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Light</th>
                                <td>
                                    ON/OFF
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Boost</th>
                                <td>4-6 kph vehicle push function</td>
                            </tr>
                            <tr>
                                <th scope="row">USB Max. Charging Current</th>
                                <td>500 mA</td>
                            </tr>
                            <tr>
                                <th scope="row">USB Charging Voltage</th>
                                <td>5V</td>
                            </tr>
                            <tr>
                                <th scope="row">Dimention</th>
                                <td>73 X 53 X 16 mm</td>
                            </tr>
                            <tr>
                                <th scope="row">Screw torque</th>
                                <td>2.9kgf-cm</td>
                            </tr>
                            <tr>
                                <th scope="row">IP Rating</th>
                                <td>55</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- table -->

<!-- af -->
<section class="py-lg-5 py-4" id="product3">
    <div id="mpf3" class="container py-md-5">
        <div class="title text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">AF</h3>
        </div>
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6"></li>
            <li class="col-lg-4 col-sm-6">
                <div class="wow fadeInUp" data-wow-duration="2s">
                    <a href="{{asset('images/dis1.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/dis1.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                </div>
            </li>
            <li class="col-lg-4 col-sm-6"></li>
        </ul>
    </div>
</section>
<!-- af -->


@elseif($series=='i4us')
<section class="contact-wthree py-sm-5 py-4" id="table">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt">product details</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h5 class="cont-form" data-blast="color">I4US</h5>
                <div class="contact-form-wthreelayouts">
                    <table class="table table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">I4US Series</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Support level</th>
                                <td>
                                    10 steps
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Boost</th>
                                <td>4-6 kph vehicle push function</td>
                            </tr>
                            <tr>
                                <th scope="row">Display I4US Dimention</th>
                                <td>80x60x16mm</td>
                            </tr>
                            <tr>
                                <th scope="row">Control Unit Dimention</th>
                                <td>48x44x16mm</td>
                            </tr>
                            <tr>
                                <th scope="row">Screw torque</th>
                                <td>2.92kgf-cm</td>
                            </tr>
                            <tr>
                                <th scope="row">Micro USB interface</th>
                                <td>charging device for mobiles and smart phones</td>
                            </tr>
                            <tr>
                                <th scope="row">USB Max. Charging Current</th>
                                <td>500 mA</td>
                            </tr>
                            <tr>
                                <th scope="row">USB Charging Voltage</th>
                                <td>5V</td>
                            </tr>
                            <tr>
                                <th scope="row">IP Rating</th>
                                <td>65</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- table -->

<!-- i4us -->
<section class="py-lg-5 py-4" id="product3">
    <div id="mpf3" class="container py-md-5">
        <div class="title text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">I4US</h3>
        </div>
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6"></li>
            <li class="col-lg-4 col-sm-6">
                <div class="wow fadeInUp" data-wow-duration="2s">
                    <a href="{{asset('images/dis2.jpg')}}" data-toggle="lightboxnew1" data-gallery="gallery">
                        <img src="{{asset('images/dis2.jpg')}}" class="ww img-fluid img-thumbnail">
                    </a>
                </div>
            </li>
            <li class="col-lg-4 col-sm-6"></li>
        </ul>
    </div>
</section>
<!-- i4us -->

@endif
@endsection