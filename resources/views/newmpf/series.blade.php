@extends('newmpf.main')
@section('content')
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="series">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF inside®</h3>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/mi7.jpg')}}" alt="mpf inside">
            </div>
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/mi6.jpg')}}" alt="mpf inside">
            </div>
        </div>
    </div>
</section>
@endsection