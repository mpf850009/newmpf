@extends('newmpf.main')
@section('content')
<style>
    .w100{
        width: 100%;
    }
    .vv{
        width: 100%;
        padding:5px 5px;
        border-color:#0099ff;
        border-width:3px;
    }
</style>

<script type="text/javascript" src="{{asset("js/mapdata.js")}}"></script>
<script type="text/javascript" src="{{asset("js/countrymap.js")}}"></script>

<!-- rentalmap -->
<section class="team py-4 py-lg-5" id="taiwan" style="background-color:black">
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
            <h3 style="color:#0099ff" class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">E-Bike Rental Location</h3>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-8" id="map"></div>
            <div class="col-md-3"></div>
            <div class="clearfix"></div>
        </div><br><br>
        <div class="row">
            <div class="col-md-4">
                <video class="vv contact-form-wthreelayouts wow jackInTheBox" controls>
                    <source src="{{asset('videos/mpfbikerental1.mp4')}}" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
</section>
<!-- rentalmap -->

@endsection