@extends('newmpf.main')
@section('content')
<!-- company -->
<div class="w3lspvt-about py-md-5 py-5" id="services">
    <div class="container pt-lg-5">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">穩正企業股份有限公司</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">Unique Product & Design Co., Ltd.</p>
        </div>
        <div class="w3lspvt-about-row row text-center pt-md-0 pt-5 mt-lg-5">
            <div class="col-sm-6 w3lspvt-about-grids">
                <div class="p-md-5 p-sm-3">
                    <span class="fa fa-map-marker wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-blast="color" data-wow-duration="2s">Location</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">成立於1989年，公司位於台灣台南市永康區，是一家助動自行車中置馬達的專業研發設計製造公司。</p>
                </div>
            </div>
            <div class="col-sm-6 w3lspvt-about-grids text-center border-left my-sm-0 my-5">
                <div class="p-md-5 p-sm-3">
                    <span class="fa fa-check-circle-o wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-blast="color" data-wow-duration="2s">Quality</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">從產品的設計、研發到生產都是一貫作業，並採用高科技的生產設備來進行產品的生產與製造，以維持產品的高品質。</p>
                </div>
            </div>
        </div>
        <div class="w3lspvt-about-row border-top row text-center pt-md-0 pt-5 mt-md-0 mt-5">
            <div class="col-sm-6 w3lspvt-about-grids">
                <div class="p-md-5 p-sm-3 col-label">
                    <span class="fa fa-paw wow flip" data-wow-duration="2s" data-blast="borderColor"></span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-wow-duration="2s" data-blast="color">reliable</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">「立足台灣，佈局全球」是穩正企業長期的發展策略。為確保台灣的技術優勢成為研發設計與生產高附加價值產品的據點。 積極建構以台灣為主軸的研發中心，整合美洲、歐洲和亞洲等各知名品牌車廠，成就全球研發、製造與銷售的強勢競爭力。</p>
                </div>
            </div>
            <div class="col-sm-6 w3lspvt-about-grids mt-lg-0 mt-md-3 border-left pt-sm-0 pt-5">
                <div class="p-md-5 p-sm-3 col-label">
                    <span class="fa fa-handshake-o wow flip" data-wow-duration="2s" data-blast="borderColor">
                    </span>
                    <h4 class="mt-2 mb-3 wow fadeInUp" data-wow-duration="2s" data-blast="color">partner</h4>
                    <p class="text-left wow fadeInUp" data-wow-duration="2s">長期以來，我們憑藉價值行銷、研究創新、注重品質與客戶服務於市場上，不斷提昇競爭力並取得眾多客戶之信任，進而創造公司與客戶雙贏局面，公司得以不斷成長！</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- company -->

<!-- portfolio -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="portfolio">
    <div class="container-fluid">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">photo</h3>
            <p class="wow fadeInUp" data-wow-duration="2s"></p>
        </div>
        <ul class="demo row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow bounceInRight" data-wow-duration="2s">
                    <img src="images/c2.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow bounceInUp" data-wow-duration="2s">
                    <img src="images/banner1.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6 mx-auto">
                <div class="gallery-grid1 wow bounceInLeft" data-wow-duration="2s">
                    <img src="images/c4.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow bounceInRight" data-wow-duration="2s">
                    <img src="images/pic4.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow bounceInDown" data-wow-duration="2s">
                    <img src="images/pic5.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow bounceInUp" data-wow-duration="2s">
                    <img src="images/pic6.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow bounceInLeft" data-wow-duration="2s">
                    <img src="images/pic7.jpg" alt=" " class="img-fluid img-thumbnail" />
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- portfolio -->

@endsection