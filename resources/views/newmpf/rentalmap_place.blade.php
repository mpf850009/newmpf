@extends('newmpf.main')
@section('content')

<style>
	.container h2,h3,h4,h5{
		font-weight: bold;
	}
    .vv{
        width: 100%;
        padding:5px 5px;
        border-color:#0099ff;
        border-width:3px;
    }
	.myImg{
		opacity: 0.6;
		-webkit-transform: scale(1);
		transform: scale(1);
		-webkit-transition: .5s ease-in-out;
		-moz-transition: .5s ease-in-out;
		-o-transition: .5s ease-in-out;
		transition: .5s ease-in-out;
	}
	.myImg:hover {
		opacity: 1;
		-webkit-transform: scale(1.2);
		transform: scale(1.2);
	}
	figure.snip1104 {
		padding: 0 0;
		margin: 0 0;
		font-family: 'Raleway', Arial, sans-serif;
		position: relative;
		overflow: hidden;
		width: 100%;
		background: #000000;
		color: #ffffff;
		text-align: center;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
	}

	figure.snip1104 * {
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: all 0.4s ease-in-out;
		transition: all 0.4s ease-in-out;
	}

	figure.snip1104 img {
		max-width: 100%;
		position: relative;
		opacity: 0.4;
	}

	figure.snip1104 figcaption {
		padding: 0 0;
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
	}

	figure.snip1104 h2 {
		position: absolute;
		left: 40px;
		right: 40px;
		display: inline-block;
		background: #000000;
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		padding: 5px 5px;
		margin: 0 0;
		top: 50%;
		text-transform: uppercase;
		font-weight: 400;
	}

	figure.snip1104 h2 span {
		font-weight: 800;
	}

	figure.snip1104:before {
		height: 100%;
		width: 100%;
		top: 0;
		left: 0;
		content: '';
		background: #ffffff;
		position: absolute;
		-webkit-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		-webkit-transform: rotate(110deg) translateY(-50%);
		transform: rotate(110deg) translateY(-50%);
	}

	figure.snip1104 a {
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		position: absolute;
		z-index: 1;
	}
	figure.snip1104:hover img,
	figure.snip1104.hover img {
		opacity: 1;
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
	}

	figure.snip1104:hover h2,
	figure.snip1104.hover h2 {
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
	}

	figure.snip1104:hover:before,
	figure.snip1104.hover:before {
		-webkit-transform: rotate(110deg) translateY(-150%);
		transform: rotate(110deg) translateY(-150%);
	}
</style>

<section class="team py-4 py-lg-5" id="place">
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
			<br>
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">{{$placename}}</h3>
		</div>
		<div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
				@if($place=="fulong")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc1.jpg')}}" alt="穩正 MPF Drive 台北 福隆驛站 fulong Taipei 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>福隆驛站Fulong Station</h3><br>
					<h4>營業時間</h4>
					<p>
						週一至週五 09:00-18:00<br>
						週六至週日 08:30-18:00<br>
					</p><br>
					<h4>聯絡電話</h4>
					<p>0905-367-700 顏敏如</p><br>
					<h4>地址</h4>
					<p>新北市貢寮區福隆街2巷1-1號</p><br>
					<h4>Facebook</h4>
					<p><a target="_blank" href="https://zh-tw.facebook.com/hpbike/">福隆驛站</a></p><br>
					<h4>官方網站</h4>
					<p><a target="_blank" href="http://www.ifulong.com.tw/">福隆驛站</a></p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.535661373456!2d121.94298461441527!3d25.01588938398086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x345d5c5484b55c5b%3A0x50fe76487401c49f!2zTVBGIERyaXZlIOemj-mahuermQ!5e0!3m2!1szh-TW!2stw!4v1543820591262" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>	
				@endif

				@if($place=="yuli")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc2.jpg')}}" alt="穩正 MPF Drive 花蓮 玉里 Hualien yuli 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>花蓮玉里站Yuli Station</h3><br>
					<h4>營業時間</h4>
					<p>09:00-17:00 每日</p><br>
					<h4>聯絡電話</h4>
					<p>0963667167</p><br>
					<h4>地址</h4>
					<p>花蓮縣玉里鎮康樂街39號</p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3663.5655101578673!2d121.30952621437494!3d23.33151808479743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f6a7ce441a1dd%3A0x7545c3b51030d4e5!2zOTgx6Iqx6JOu57ij546J6YeM6Y6u5bq35qiC6KGXMznomZ8!5e0!3m2!1szh-TW!2stw!4v1543825372505" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif

				@if($place=="chishang")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc3.jpg')}}" alt="穩正 MPF Drive 台東 池上 Taitung Chishang 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>台東池上站Chishang Station</h3><br>
					<h4>營業時間</h4>
					<p>
						週一至週四 06:00-17:00<br>
						　　　週五 05:00-17:00<br>
						週六至週日 06:00-17:00<br>
					</p><br>
					<h4>聯絡電話</h4>
					<p>0919-139020 邱佩馨<br>0919-139720 林涂仲</p><br>
					<h4>地址</h4>
					<p>958台東縣池上鄉中正路48號</p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14677.006217253087!2d121.2209947!3d23.1244787!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc56a24131d6739f3!2z5rGg5LiK55Kw6Y6u6Ieq6KGM6LuK5bqX!5e0!3m2!1szh-TW!2stw!4v1543824232686" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif

				@if($place=="asheng")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc4.jpg')}}" alt="穩正 MPF Drive 台東 阿勝單車 Taitung A-Sheng 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>台東阿勝租車A-Sheng Station</h3><br>
					<h4>營業時間</h4>
					<p>09:00-18:00 每日</p><br>
					<h4>聯絡電話</h4>
					<p>0982-158153</p><br>
					<h4>地址</h4>
					<p>台東市新站路223號</p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5201.825622362743!2d121.12384078743067!3d22.793340483517955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346fba2d2dfccf93%3A0x8f4a7a4181d3c52e!2z6Zi_5Yud5Zau6LuK5Y-w5p2x5bqX!5e0!3m2!1szh-TW!2stw!4v1543826324832" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif

				@if($place=="yongkang")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc5.jpg')}}" alt="穩正 MPF Drive 台南 永康 樂享學 Tainan Yongkang ofami 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>樂享學O-Fami</h3><br>
					<h4>營業時間</h4>
					<p>
						一 10:00-18:00<br>
						二 10:00-18:00<br>
						三 10:00-18:00<br>
						四 公休<br>
						五 10:00-18:00<br>
						六 09:00-17:00<br>
						日 09:00-17:00<br>
					</p><br>
					<h4>聯絡電話</h4>
					<p>06-2028028</p><br>
					<h4>地址</h4>
					<p>台南市永康區中山路298號</p><br>
					<h4>Facebook</h4>
					<p><a target="_blank" href="https://www.facebook.com/o.fami.tw/">樂享學O-Fami</a></p><br>
					<h4>Instagram</h4>
					<p><a target="_blank" href="https://www.instagram.com/o.fami.tw/">樂享學O-Fami</a></p><br>
					<h4>官方網站</h4>
					<p><a target="_blank" href="https://www.o-fami.com.tw/">樂享學O-Fami</a></p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908445!2d120.2532284143682!3d23.03299068494779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1543818683010" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif

				@if($place=="zuojhen")
					@include("newmpf.facebook")
					<a class="wow fadeInUp" data-wow-duration="2s">
						<img class="img-responsive" width="100%" src="{{asset('images/zuojhen/bike1.jpg')}}" alt="穩正 MPF Drive 台南 左鎮 月世界旅遊服務中心 zuojhen tainan 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">營業時間</h4>
					<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">08:00-17:00</p><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">聯絡方式</h4>
					<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">聯絡人：陳柳足<br>電話：06-5730107<br>E-mail：<a href="mailto:chenwinding@yahoo.com.tw">chenwinding@yahoo.com.tw</a></p><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">地址</h4>
					<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">台南市左鎮區岡林里31號</p><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">官方網站</h4>
					<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s"><a target="_blank" href="https://www.twtainan.net/zh-tw/Attractions/Detail/4358/">左鎮月世界生態學園(岡林國小)</a></p><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">Facebook</h4>
					<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s"><a target="_blank" href="https://zh-tw.facebook.com/pages/category/Social-Service/%E5%8F%B0%E5%8D%97%E5%B8%82%E5%B7%A6%E9%8E%AE%E5%8D%80%E5%85%AC%E8%88%98%E7%A4%BE%E5%8D%80-199524760230777/">台南市左鎮區公舘社區發展協會</a></p><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">Google Map</h4><br>
					<iframe class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.4518689766755!2d120.41239661496752!3d23.007175284960837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6a14028f8fd3d557!2z5Y-w5Y2X5biC5bem6Y6u5Y2A5YWs6IiY56S-5Y2A55m85bGV5Y2U5pyD!5e0!3m2!1szh-TW!2stw!4v1565145603016!5m2!1szh-TW!2stw" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br><br>
					<h5 class="wow fadeInUp" data-wow-duration="2s">Photo</h4><br>
					<!-- portfolio -->
					<section class="wthree-row w3-gallery cliptop-portfolio-wthree">
						<div class="container-fluid">
							<ul class="demo row pb-4">
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/bike1.jpg')}}" title="電助車出租" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;"/>
											<figcaption style="padding:0 0;">
												<h2>電助車出租<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/sunrise1.jpg')}}" title="二寮日出" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>二寮日出<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/sunrise2.jpg')}}" title="二寮日出" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>二寮日出<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/mountain1.jpg')}}" title="月世界尖峰" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>月世界尖峰<span></span></h2>
											</figcaption>
										</figure>		
									</div>							
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/canyon1.jpg')}}" title="月世界大峽谷" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>月世界大峽谷<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/308.jpg')}}" title="308高地" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>308高地<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/church1.jpg')}}" title="岡林教會" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>岡林教會<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/oldhouse1.jpg')}}" title="李家古厝" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>李家古厝<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
								<li class="col-lg-4 col-sm-6" style="padding:0 0;">
									<div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
										<figure class="snip1104">
											<img src="{{asset('images/zuojhen/pipehouse1.jpg')}}" title="水管屋" alt="電助車出租 二寮 左鎮 台南 租車 腳踏車 日出 月世界 大峽谷 308 高地 岡林教會 李家古厝" style="padding:0 0;" />
											<figcaption style="padding:0 0;">
												<h2>水管屋<span></span></h2>
											</figcaption>
										</figure>		
									</div>
								</li>
							</ul>
						</div>
					</section>
					<!-- portfolio -->
				@endif
				
				@if($place=="fengyuan")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc7.jpg')}}" alt="穩正 MPF Drive 台中 起站租車 Taichung Fengyuan 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>起站租車Fengyuan Station</h3><br>
					<h4>營業時間</h4>
					<p>
						一 09:00-18:30<br>
						二 公休<br>
						三 公休<br>
						四 09:00-18:30<br>
						五 09:00-18:30<br>
						六 09:00-18:30<br>
						日 09:00-18:30<br>
					</p><br>
					<h4>聯絡電話</h4>
					<p>
						04-25155903<br>
						0929-686135
					</p><br>
					<h4>地址</h4>
					<p>420台中市豐原區豐勢路一段103號</p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3148.5236106515213!2d120.72245008561092!3d24.25383567038971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34691b0292bd26e9%3A0x91459572e51778cc!2z6LW356uZ56ef6LuKLembu-WLleiHquihjOi7iuWfjuW4guaXhemBig!5e0!3m2!1szh-TW!2stw!4v1543821780427" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif
				
				@if($place=="houli")
					<a class="wow fadeInUp" data-wow-duration="2s" href="#">
						<img class="img-responsive" width="100%" src="{{asset('images/loc8.jpg')}}" alt="穩正 MPF Drive 台中 后里 奇典租車 Taichung Houli 電動輔助自行車 租車 腳踏車">
					</a>
					<br><br>
					<h3>奇典租車</h3><br>
					<h4>營業時間</h4>
					<p>07:00-20:00</p><br>
					<h4>聯絡方式</h4>
					<p>電話：0930-681103</p><br>
					<h4>地址</h4>
					<p>421台中市后里區甲后路9號</p><br>
					<h4>官方網站</h4>
					<p><a target="_blank" href="https://chidian.mmweb.tw/?fbclid=IwAR0_dpIgE3RZJmGwJ2u-MgLoGC8kO8KAAsIC8IlHl_Ush9qzz9HpXan-0Ps">后豐鐵馬道.奇典租車中心</a></p><br>
					<h4>Facebook</h4>
					<p><a target="_blank" href="https://www.facebook.com/chidian2/">后豐鐵馬道.奇典租車中心</a></p><br>
					<h4>Google Map</h4><br>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d909.0162423694995!2d120.73179572915456!3d24.309368799019797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34691b4b7d8e6477%3A0xbb6d1a6cd69d530c!2z5aWH5YW456ef6LuK5Lit5b-DICjlkI7osZDpkLXppqzpgZPlj4rmnbHosZDntqDoibLotbDlu4rnp5_ou4op!5e0!3m2!1szh-TW!2stw!4v1549868886529" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br>
				@endif
            </div>
        </div>
	</div>
</section>

@endsection