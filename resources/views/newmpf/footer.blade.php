<!-- footer -->
<footer class="cpy-right bg-theme" data-blast="bgColor">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="wthree-social">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/MPFDrive/" target="_blank" class="wow bounceInDown" data-wow-duration="2s">
                                <span class="fa fa-facebook-f icon_facebook"></span>
                            </a>
                            <a href="https://www.youtube.com/channel/UCMlxuUdGHpY-EJ5g6ANO7dQ" target="_blank" class="wow bounceInDown" data-wow-duration="2s">
                                <span class="fa fa-youtube icon_g_plus"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 text-lg-right mt-lg-0 mt-4">
                <p class="wow fadeInUp" data-wow-duration="2s">© Copyright(C) 2018 Unique Product & Design Co., Ltd. All Rights Reserved.</a>
                </p>
                <p class="wow fadeInUp" data-wow-duration="2s">Layout is Designed by
                    <a href="http://w3layouts.com"> W3layouts.</a>
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->