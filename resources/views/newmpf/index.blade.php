@extends('newmpf.main')
@section('content')

<style>
    .pad0{
        padding: 0px 0px;
    }
    .pad10{
        padding:10px 10px;
    }
    .pad15{
        padding:15px 15px;
    }
    .vv{
        width: 100%;
        padding:15px 15px;
        border-color:#0099ff;
        border-width:3px;
        border-radius: 30px;
    }
    .ww{
        width: 100%;
        padding:0 0;
        border-color:#0099ff;
        border-width:3px;
        border-radius: 30px;
    }
</style>

@include('newmpf.prod')

{{--  MPF Series  --}}
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="series">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">MPF Drive Series</h3>
        </div>
        <div class="container pt-sm-5 pt-4">
            <div class="col-md-12">
                <img class="img-responsive" width="100%" src="{{asset('images/mi10.jpg')}}" alt="mpf inside">
            </div>
        </div>
    </div>
</section>
{{--  MPF Series  --}}

<!--  about -->
<section class="wthree-slide-btm pt-lg-5" id="about">
    <div class="container pt-sm-5 pt-4">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div class="slide-banner wow fadeIn" data-wow-duration="2s">
                </div>
            </div>
            <div class="col-lg-8">
                <div class="bg-abt light-bg">
                    <div class="container">
                        <div class="title-desc  pb-sm-3">
                            <h4 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">ABOUT MPF</h4>
                            <p></p>
                        </div>
                        <div class="row flex-column mt-lg-4 mt-3">
                            <div class="abt-grid">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="abt-icon wow fadeInUpDownLeft" data-wow-duration="2s" data-blast="bgColor">
                                            <span class="fa fa-bicycle"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pl-sm-0">
                                        <div class="abt-txt ml-sm-0 wow fadeInUp" data-wow-duration="2s">
                                            <h4>&emsp;&emsp;成立於1989年，公司位於台灣台南市永康區，是一家助動自行車中置馬達的專業研發設計製造公司。從產品的設計、研發到生產都是一貫作業，並採用高科技的生產設備來進行產品的生產與製造，以維持產品的高品質。</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="abt-grid mt-4 pt-lg-2">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="abt-icon wow fadeInUpDownRight" data-wow-duration="2s" data-blast="bgColor">
                                            <span class="fa fa-cogs"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pl-sm-0">
                                        <div class="abt-txt ml-sm-0 wow fadeInUp" data-wow-duration="2s">
                                            <h4>&emsp;&emsp;「立足台灣，佈局全球」是穩正企業長期的發展策略。為確保台灣的技術優勢成為研發設計與生產高附加價值產品的據點。 積極建構以台灣為主軸的研發中心，整合美洲、歐洲和亞洲等各知名品牌車廠，成就全球研發、製造與銷售的強勢競爭力。長期以來，我們憑藉價值行銷、研究創新、注重品質與客戶服務於市場上，不斷提昇競爭力並取得眾多客戶之信任，進而創造公司與客戶雙贏局面，公司得以不斷成長！</h4>
                                        </div>
                                    </div>
                                </div>
                            </div><br><br><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about -->

<!--  about bottom -->
<section class="wthree-slide-btm pb-lg-5">
    <div class="container pb-md-5 pb-4">
        <div class="row flex-row-reverse no-gutters">
            <div class="col-lg-4">
                <div class="ab-banner wow fadeIn" data-wow-duration="2s"></div>
            </div>
            <div class="col-lg-8">
                <div class="bg-abt">
                    <div class="container">
                        <div class="title-desc  pb-sm-3">
                            <h4 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">FUTURE</h4>
                            <p></p>
                        </div>
                        <div class="row flex-column mt-lg-4 mt-3">
                            <div class="abt-grid">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="abt-icon wow fadeInUpDownLeft" data-wow-duration="2s" data-blast="bgColor">
                                            <span class="fa fa-globe"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pl-sm-0">
                                        <div class="ml-sm-0 wow fadeInUp" data-wow-duration="2s">
                                            <h4 style="font-size:20px">&emsp;&emsp;MPF Drive – Unique Product & Design Co., Ltd. Founded in 1986 and located in Yong Kang Dist., Tainan,Taiwan. Unique is a professional mid motor manufacturer. We adopt high-tech equipment from R&D to production process to maintain the high quality of our products. </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="abt-grid mt-4 pt-lg-2">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="abt-icon wow fadeInUpDownRight" data-wow-duration="2s" data-blast="bgColor">
                                            <span class="fa fa-balance-scale"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pl-sm-0">
                                        <div class="ml-sm-0 wow fadeInUp" data-wow-duration="2s">
                                            <h4 style="font-size:20px">&emsp;&emsp;Over the thirty years, we have been focusing on value marketing, innovation, product quality and customer service, obtaining customer&#39;s trust to create a win-win situation. Thus we could enhance our competitiveness and keep growing up. </h4>
                                        </div>
                                    </div>
                                </div>
                            </div><br><br>
                            {{-- <div class="text-right">
                                <br><a class="btn bg-theme mt-3 w3_pvt-link-bnr wow jackInTheBox" data-wow-duration="2s" data-blast="bgColor" href="{{route('company')}}" role="button">Explore <span class="fa fa-search"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about boottom -->

<!-- portfolio -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="portfolio">
    <div class="container-fluid">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">photo</h3>
            <p class="wow fadeInUp" data-wow-duration="2s"></p>
        </div>
        <ul class="demo row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic1.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic2.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic3.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-3 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic4.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic5.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic6.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
            <li class="col-lg-4 col-sm-6 mx-auto">
                <div class="gallery-grid1 wow fadeInUp pad15" data-wow-duration="2s">
                    <img src="images/pic7.jpg" alt=" " class="vv img-fluid img-thumbnail" />
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- portfolio -->

<!-- logo -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="logo">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">partner</h3>
        </div>
        <ul class="row py-lg-5 py-sm-4 pb-4">
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl6">
                        <img src="images/pl6.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl1">
                        <img src="images/pl1.jpg" alt="mpf drive manta5 partner" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl2">
                        <img src="images/pl2.jpg" alt="mpf drive ceclo" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl3">
                        <img src="images/pl3.jpg" alt="mpf drive pelican-bike" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl4">
                        <img src="images/pl4.jpg" alt="mpf drive igo bike" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl5">
                        <img src="images/pl5.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl7">
                        <img src="images/pl7.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="{{route('partner')}}#pl8">
                        <img src="images/pl8.png" alt="mpf drive" class="ww img-fluid img-thumbnail" style="background-color: black" />
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl9.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl10.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl11.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl12.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl13.png" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl15.png" alt="mpf drive" class="ww img-fluid img-thumbnail" style="background-color: black"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl16.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl17.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl18.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl19.png" alt="mpf drive" class="ww img-fluid img-thumbnail" style="background-color: black"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl20.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl21.png" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl22.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl23.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl24.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl25.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl26.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
            <li class="col-lg-2 col-sm-4 pad10">
                <div class="gallery-grid1 wow fadeInUp" data-wow-duration="2s">
                    <a href="#">
                        <img src="images/pl27.jpg" alt="mpf drive" class="ww img-fluid img-thumbnail"/>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- logo -->

    <!-- contact -->
    <section class="contact-wthree py-sm-5 py-4" id="contact">
        <div class="container pt-lg-5">
            <div class="title-desc text-center pb-sm-3">
                <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">contact us</h3>
                <p class="wow fadeInUp" data-wow-duration="2s">Experience & improve on bike issues.</p>
                <h5 class="cont-form wow fadeInUp" data-wow-duration="2s" data-blast="color">get in touch</h5>
            </div>
            <div class="row mt-2">
                <div class="col-lg-3 text-center">
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <a href="mailto:ec@mpf.tw">
                                <span class="fa fa-envelope-open mb-3 wow flip" data-blast="color"></span>
                            </a>
                            <div class="d-flex flex-column">
                                <p class="wow fadeInUp" data-wow-duration="2s">
                                    資深副總<br>Senior Vice General Manager<br>
                                    陳鵬旭 Eddy Chen <br>
                                    Mobile: + 886-910-106427 <br>
                                    E-Mail: <a href="mailto:ec@mpf.tw">ec@mpf.tw</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <a href="mailto:eric.hsieh@mpf.tw">
                                <span class="fa fa-envelope-open mb-3 wow flip" data-blast="color"></span>
                            </a>
                            <div class="d-flex flex-column">
                                <p class="wow fadeInUp" data-wow-duration="2s">
                                    業務行銷副總<br>Sales Vice General Manager<br>
                                    謝宗達 Eric Hsieh<br>
                                    Mobile: + 886-933-200618 <br>
                                    E-Mail: <a href="mailto:eric.hsieh@mpf.tw">eric.hsieh@mpf.tw</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <span class="fa fa-phone mb-3 wow flip" data-blast="color"></span>
                            <div class="d-flex flex-column wow fadeInUp" data-wow-duration="2s">
                                <p>TEL : +886-6-253-8390</p>
                                <p>FAX : +886-6-254-1507</p>
                                <p>E-Mail : <a href="mailto:sales@mpf.tw">sales@mpf.tw</a></p>
                                <p>　</p><p>　</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <span class="fa fa-home mb-3 wow flip" data-blast="color"></span>
                            <div class="d-flex flex-column">
                                <p class="wow fadeInUp" data-wow-duration="2s">Unique Product & Design Co., Ltd.<br>穩正企業股份有限公司<br>No. 5, Ming Dong Road, Yong Kang District,Tainan,Taiwan(ROC). <br>台南市永康區民東路5號</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="mx-auto text-center mt-lg-0 mt-4">
                <iframe class="mt-lg-4 contact-form-wthreelayouts wow jackInTheBox" data-wow-duration="2s" data-blast="borderColor" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14684.78059868254!2d120.259536!3d23.053306!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4c4704ff65de7975!2z56mp5q2j5LyB5qWt6IKh5Lu95pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1545378575700"
                    allowfullscreen></iframe>
                <!-- footer right -->
            </div>
        </div>
    </section>
    <!-- contact -->
@endsection