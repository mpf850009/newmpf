@extends('newmpf.main')
@section('content')
<!-- news -->
<section class="wthree-row w3-gallery cliptop-portfolio-wthree py-lg-5 py-4" id="news">
    <div class="container-fluid py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">News</h3>
        </div>
        <div class="container pt-sm-5 pt-4">
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20220309"])}}"><i class="fa fa-arrow-right hvr-icon"></i> 03/09-03/12 2022台北國際自行車展覽會 南港展覽館1館 </a></h5><br>
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20200304"])}}"><i class="fa fa-arrow-right hvr-icon"></i> 03/04-03/07 2020台北國際自行車展覽會 南港展覽館2館 </a></h5><br>
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20191016"])}}"><i class="fa fa-arrow-right hvr-icon"></i> 10/16-10/18 2019台中週自行車 Taichung Bike Week  </a></h5><br>
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20190408"])}}"><i class="fa fa-arrow-right hvr-icon"></i> 感謝台北展期間的雜誌報導 </a></h5><br>
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20190318"])}}"><i class="fa fa-arrow-right hvr-icon"></i> MPF6系列通過EN15194(13849)/2017認證 </a></h5><br>
            <h5><a class="hvr-icon-wobble-horizontal text-left wow fadeInUp" data-wow-duration="2s" href="{{route("news_date",[$date="20190312"])}}"><i class="fa fa-arrow-right hvr-icon"></i> 03/27-03/30 2019台北國際自行車展覽會 南港展覽館 </a></h5><br>
        </div>
    </div>
</section>
<!-- news -->
@endsection