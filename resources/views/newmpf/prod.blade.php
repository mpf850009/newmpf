<style>
.ww{
    width: 100%;
    padding:0 0;
    border-color:#0099ff;
    border-width:3px;
    border-radius: 30px;
}
</style>
<!-- team -->
<section class="team py-4 py-lg-5" id="team">
    <div class="container py-lg-5 py-sm-4">
        <div class="title-desc text-center pb-sm-3">
            <h3 class="main-title-w3pvt wow fadeInUp" data-wow-duration="2s">our product</h3>
            <p class="wow fadeInUp" data-wow-duration="2s">ALL NEW MPF Drive</p>
        </div>
        <div class="row py-4 mt-lg-5 team-grid">
            {{-- <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/m3.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive 5 mid motor" />
                    <div class="box-content">
                        <h4 class="title" data-blast="color">MPF DRIVE 3</h4>
                        <span class="post" style="font-size: 18px;">250W<br>40Nm<br>2.9kg<br>IP65</span>
                        <ul class="social">
                            <li><a href="{{route('product_series',[$series='3'])}}#table"><span class="fa fa-search"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div> --}}
            <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/m5.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive 5 mid motor" />
                    <div class="box-content">
                        <h4 class="title" data-blast="color" >MPF DRIVE 5</h4>
                        <span class="post" style="font-size: 18px;">250W<br>60Nm<br>5.1kg<br>IP67</span>
                        <ul class="social">
                            <li><a href="{{route('product_series',[$series='5'])}}#table"><span class="fa fa-search"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/m6sl.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive 6sl mid motor" />
                    <div class="box-content">
                        <h4 class="title" data-blast="color">MPF DRIVE 6</h4>
                        <span class="post" style="font-size: 18px;">250W<br>80-100Nm<br>4.7kg<br>IP67</span>
                        <ul class="social">
                            <li><a href="{{route('product_series',[$series='6'])}}#table"><span class="fa fa-search"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/dis1.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive" />
                    <div class="box-content">
                        <h4 class="title" data-blast="Color">Display AF</h4>
                        <span class="post" style="size:5px;">Speed<br>Distance<br>RPM<br>10 Support Level</span>
                         <ul class="social">
                            <li><a href="{{route('product_series',[$series='af'])}}#table"><span class="fa fa-search"></a></li>
                        </ul> 
                    </div>
                </div>
            </div> --}}
            <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/dis2.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive " />
                    <div class="box-content">
                        <h4 class="title" data-blast="Color">Display I4US</h4>
                        <span class="post" style="font-size: 18px;">Speed<br>Distance<br>RPM<br>10 Support Level</span>
                         <ul class="social">
                            <li><a href="{{route('product_series',[$series='i4us'])}}#table"><span class="fa fa-search"></a></li>
                        </ul> 
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s" style="padding:5px 5px;">
                <div class="box13">
                    <img src="{{asset('images/bat1.jpg')}}" class="ww img-fluid img-thumbnail" alt="mpf drive" />
                    <div class="box-content">
                        <h4 class="title" data-blast="Color">Battery</h4>
                        {{--  <ul class="social">
                            <li><a href="{{route('product_series',[$series='bat'])}}#table"><span class="fa fa-search"></span></a></li>
                        </ul>  --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- team -->