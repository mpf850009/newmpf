var simplemaps_countrymap_mapdata = {
  main_settings: {
    //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#2e9df7",
    background_transparent: "yes",
    border_color: "#FFFFFF",

    //State defaults
    state_description: "駐點中...",
    state_color: "#cccccc",
    state_hover_color: "#9f9fa3",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",

    //Location defaults
    location_description: "Location description",
    location_url: "/public/index.php/rentalmap",
    location_color: "#0099ff",
    location_opacity: "1",
    location_hover_opacity: 1,
    location_size: "20",
    location_type: "circle",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",

    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",

    //Zoom settings
    zoom: "yes",
    manual_zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,

    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",

    //Advanced settings
    div: "map",
    auto_load: "yes",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    TWN1156: {
      name: "高雄"
    },
    TWN1158: {
      name: "屏東"
    },
    TWN1160: {
      name: "台南",
      hover_color: "black",
      description: "永康、左鎮"
    },
    TWN1161: {
      name: "新竹市"
    },
    TWN1162: {
      name: "新竹"
    },
    TWN1163: {
      name: "宜蘭",
    },
    TWN1164: {
      name: "基隆市"
    },
    TWN1165: {
      name: "苗栗"
    },
    TWN1166: {
      name: "台北市"
    },
    TWN1167: {
      name: "新北市",
      hover_color: "black",
      description: "福隆"
    },
    TWN1168: {
      name: "桃園"
    },
    TWN1169: {
      name: "彰化"
    },
    TWN1170: {
      name: "嘉義"
    },
    TWN1171: {
      name: "嘉義市"
    },
    TWN1172: {
      name: "花蓮",
      hover_color: "black",
      description: "玉里"
    },
    TWN1173: {
      name: "南投"
    },
    TWN1174: {
      name: "台中",
      hover_color: "black",
      description: "豐原、后里"
    },
    TWN1176: {
      name: "雲林"
    },
    TWN1177: {
      name: "台東",
      hover_color: "black",
      description: "池上、台東"
    },
    TWN3414: {
      name: "澎湖"
    },
    TWN3415: {
      name: "金門"
    },
    TWN5128: {
      name: "連江"
    }
  },
  locations: {
    "0": {
      lat: "25.015910",
      lng: "121.945132",
      name: "福隆租車站-福隆驛站",
      url: "/public/index.php/rentalmap/fulong",
      description: "福隆租車站-福隆驛站 <img class=\"img-responsive w100\" src=\"../../public/images/loc1.jpg\" alt=\"\" srcset=\"\">"
    },
    "1": {
      lat: "23.331524",
      lng: "121.311739",
      name: "玉里租車站-關山汽車",
      url: "/public/index.php/rentalmap/yuli",
      description: "玉里租車站-關山汽車 <img class=\"img-responsive w100\" src=\"../../public/images/loc2.jpg\" alt=\"\" srcset=\"\">"
    },
    "2": {
      lat: "23.124823",
      lng: "121.220762",
      name: "池上租車站-日光車行",
      url: "/public/index.php/rentalmap/chishang",
      description: "池上租車站-日光車行 <img class=\"img-responsive w100\" src=\"../../public/images/loc3.jpg\" alt=\"\" srcset=\"\">"
    },
    "3": {
      lat: "23.032990",
      lng: "120.255404",
      name: "永康租車站-樂享學",
      url: "/public/index.php/rentalmap/yongkang",
      description: "永康租車站-樂享學 <img class=\"img-responsive w100\" src=\"../../public/images/loc5.jpg\" alt=\"\" srcset=\"\">"
    },
    "4": {
      lat: "22.792931",
      lng: "121.124954",
      name: "台東租車站-阿勝單車",
      url: "/public/index.php/rentalmap/asheng",
      description: "台東租車站-阿勝單車 <img class=\"img-responsive w100\" src=\"../../public/images/loc4.jpg\" alt=\"\" srcset=\"\">"
    },
    "5": {
      lat: "24.253838",
      lng: "120.723813",
      name: "豐原租車站-起站租車",
      url: "/public/index.php/rentalmap/fengyuan",
      description: "豐原租車站-起站租車 <img class=\"img-responsive w100\" src=\"../../public/images/loc7.jpg\" alt=\"\" srcset=\"\">"
    },
    "6": {
      lat: "23.007135",
      lng: "120.414763",
      name: "左鎮租車站-公舘社區發展協會",
      url: "/public/index.php/rentalmap/zuojhen",
      description: 'Facebook搜尋「台南市左鎮區公舘社區發展協會」 <img class="img-responsive w100" src="../../public/images/zuojhen/bike1.jpg">'
    },
    "7": {
      lat: "24.309342",
      lng: "120.732554",
      name: "后里租車站-奇典租車",
      url: "/public/index.php/rentalmap/houli",
      description: "后里租車站-奇典租車 <img class=\"img-responsive w100\" src=\"../../public/images/loc8.jpg\" alt=\"\" srcset=\"\">"
    }
  },
  labels: {},
  regions: {}
};